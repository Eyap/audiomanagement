namespace GameLibrary.AudioManagement
{
	using System;
	using UnityEngine;

	/// <summary>
	/// Represents a group of AudioClips that can be treated as one, and provides automatic randomisation or sequencing based on the <c>SequenceMode</c> value.
	/// </summary>
	[Serializable]
	public abstract class BaseAudioSO : ScriptableObject
	{
		/// <summary>
		/// Chooses the next clip in the sequence, either following the order or randomly.
		/// </summary>
		/// <returns>A reference to an AudioClip</returns>
		public abstract AudioClip GetNextClip();
	}
}

# GameLibrary - Audio Management

A Unity package that add utilities to play music effects and background.


## Dependencies 

To use this package, you also need the following :
- SO workflow, that can be added via git url with `https://gitlab.com/valentin_bourdon/ScriptableObject_Workflow.git`.
- com.bourdonvalentin.object-pooling, that can be added via git url with `https://gitlab.com/valentin_bourdon/ObjectPooling.git`.
- com.demigiant.dotween, that can be added with `https://github.com/Eyap53/dotween.git?path=/Package`.


## Adding the Package

This package can easily be added to Unity via git url with `https://gitlab.com/Eyap/audiomanagement.git`.

## License

Initial work made by [UnityTechnologies](https://github.com/UnityTechnologies) and contributors of the ChopChop game, provided under the Apache License 2.0, see [COPY](./COPY) file.
You will find the list of changes in the [CHANGELOG](./CHANGELOG.md) file.

All the files in this package are released under the MPL-2.0 License, see [LICENSE](./LICENSE.md).

## End note

I think this system can be greatly improved, and will do so when I have time... (so far in the future :) )

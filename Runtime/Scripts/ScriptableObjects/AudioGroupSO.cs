namespace GameLibrary.AudioManagement
{
	using System;
	using UnityEngine;

	/// <summary>
	/// Represents a group of AudioClips that can be treated as one, and provides automatic randomisation or sequencing based on the <c>SequenceMode</c> value.
	/// </summary>
	[Serializable]
	public class AudioGroupSO : BaseAudioSO
	{
		public SequenceMode sequenceMode = SequenceMode.RandomNoImmediateRepeat;
		public AudioClip[] audioClips;
		public bool isLooping;

		private int _nextClipToPlay = -1;
		private int _lastClipPlayed = -1;

		/// <summary>
		/// Chooses the next clip in the sequence, either following the order or randomly.
		/// </summary>
		/// <returns>A reference to an AudioClip</returns>
		public override AudioClip GetNextClip()
		{
			// Fast out if there is only one clip to play
			if (audioClips.Length == 1)
				return audioClips[0];

			if (_nextClipToPlay == -1)
			{
				// Index needs to be initialised: 0 if Sequential, random if otherwise
				_nextClipToPlay = (sequenceMode == SequenceMode.Sequential) ? 0 : UnityEngine.Random.Range(0, audioClips.Length - 1);
			}
			else
			{
				// Select next clip index based on the appropriate SequenceMode
				switch (sequenceMode)
				{
					case SequenceMode.Random:
						_nextClipToPlay = UnityEngine.Random.Range(0, audioClips.Length - 1);
						break;

					case SequenceMode.RandomNoImmediateRepeat:
						do
						{
							_nextClipToPlay = UnityEngine.Random.Range(0, audioClips.Length - 1);
						} while (_nextClipToPlay == _lastClipPlayed);
						break;

					case SequenceMode.Sequential:
						_nextClipToPlay++;
						if (_nextClipToPlay > audioClips.Length - 1)
						{
							_nextClipToPlay = 0;
						}
						break;
					default:
						throw new NotImplementedException();
				}
			}

			_lastClipPlayed = _nextClipToPlay;

			return audioClips[_nextClipToPlay];
		}

		public enum SequenceMode
		{
			Random,
			RandomNoImmediateRepeat,
			Sequential,
		}
	}
}

﻿namespace GameLibrary.AudioManagement
{
	using System;

	public struct AudioCueKey : IEquatable<AudioCueKey>
	{
		public static readonly AudioCueKey Invalid = new AudioCueKey(-1, null);

		internal int value;
		internal BaseAudioSO audioCue;

		internal AudioCueKey(int value, BaseAudioSO audioCue)
		{
			this.value = value;
			this.audioCue = audioCue;
		}

		public override bool Equals(object other)
		{
			if (ReferenceEquals(null, other))
				return false;
			if (ReferenceEquals(this, other))
				return true;
			if (other is not AudioCueKey)
				return false;
			return IsEqual((AudioCueKey)other);
		}
		public bool Equals(AudioCueKey other)
		{
			if (ReferenceEquals(this, other))
				return true;
			return IsEqual(other);
		}

		public override int GetHashCode()
		{
			return value.GetHashCode() ^ audioCue.GetHashCode();
		}
		public static bool operator ==(AudioCueKey left, AudioCueKey right)
		{
			return left.Equals(right);
		}
		public static bool operator !=(AudioCueKey left, AudioCueKey right)
		{
			return !left.Equals(right);
		}

		/// <summary>
		/// Compare this object with other.
		/// </summary>
		private bool IsEqual(AudioCueKey other)
		{
			return this.value == other.value && this.audioCue == other.audioCue;
		}
	}
}

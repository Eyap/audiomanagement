﻿namespace GameLibrary.AudioManagement
{
	using DG.Tweening;
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.Events;

	[RequireComponent(typeof(AudioSource))]
	public class SoundEmitter : MonoBehaviour
	{
		private AudioSource _audioSource;

		public AudioGroupSO audioGroupSO;
		public AudioConfigurationSO audioConfigurationSO;

		public event UnityAction<SoundEmitter> OnSoundFinishedPlaying;
		public event UnityAction<SoundEmitter> OnSoundLoopNext;

		private void Awake()
		{
			_audioSource = this.GetComponent<AudioSource>();
			_audioSource.playOnAwake = false;
		}

		/// <summary>
		/// Instructs the AudioSource to play a single clip, with optional looping, in a position in 3D space.
		/// </summary>
		/// <param name="clip"></param>
		/// <param name="settings"></param>
		/// <param name="hasToLoop"></param>
		/// <param name="position"></param>
		public void PlayAudioClip(AudioClip clip, AudioConfigurationSO settings, bool hasToLoop, Vector3 position = default)
		{
			if (clip is null)
			{
				throw new ArgumentNullException(nameof(clip));
			}

			if (settings is null)
			{
				throw new ArgumentNullException(nameof(settings));
			}

			_audioSource.clip = clip;
			settings.ApplyTo(_audioSource);
			_audioSource.transform.position = position;
			_audioSource.loop = hasToLoop;
			_audioSource.time = 0f; //Reset in case this AudioSource is being reused for a short SFX after being used for a long music track
			_audioSource.Play();

			if (!hasToLoop)
			{
				StartCoroutine(FinishedPlaying(clip.length, NotifyBeingDone));
			}
			else
			{
				StartCoroutine(FinishedPlaying(clip.length, NotifySoundLoopNext));
			}
		}

		public void FadeMusicIn(AudioClip musicClip, AudioConfigurationSO settings, float duration, float startTime = 0f)
		{
			if (musicClip is null)
			{
				throw new ArgumentNullException(nameof(musicClip));
			}

			if (settings is null)
			{
				throw new ArgumentNullException(nameof(settings));
			}

			if (duration < 0f)
			{
				throw new ArgumentNullException(nameof(duration));
			}

			PlayAudioClip(musicClip, settings, true);
			_audioSource.volume = 0f;

			//Start the clip at the same time the previous one left, if length allows
			//TODO: find a better way to sync fading songs
			if (startTime <= _audioSource.clip.length)
				_audioSource.time = startTime;

			_audioSource.DOFade(settings.volume, duration);
		}

		public float FadeMusicOut(float duration)
		{
			_audioSource.DOFade(0f, duration).onComplete += OnFadeOutComplete;

			return _audioSource.time;
		}

		private void OnFadeOutComplete()
		{
			NotifyBeingDone();
		}

		/// <summary>
		/// Used to check which music track is being played.
		/// </summary>
		public AudioClip GetClip()
		{
			return _audioSource.clip;
		}


		/// <summary>
		/// Used when the game is unpaused, to pick up SFX from where they left.
		/// </summary>
		public void Resume()
		{
			_audioSource.Play();
		}

		/// <summary>
		/// Used when the game is paused.
		/// </summary>
		public void Pause()
		{
			_audioSource.Pause();
		}

		public void Stop()
		{
			_audioSource.Stop();
			OnSoundFinishedPlaying = null;
			OnSoundLoopNext = null;
		}

		public void Finish()
		{
			if (_audioSource.loop)
			{
				_audioSource.loop = false;
				float timeRemaining = _audioSource.clip.length - _audioSource.time;
				StartCoroutine(FinishedPlaying(timeRemaining, NotifyBeingDone));
			}
		}

		public bool IsPlaying()
		{
			return _audioSource.isPlaying;
		}

		public bool IsLooping()
		{
			return _audioSource.loop;
		}

		IEnumerator FinishedPlaying(float clipLength, Action callback)
		{
			yield return new WaitForSeconds(clipLength);

			if (callback != null)
			{
				callback();
			}
		}

		private void NotifyBeingDone()
		{
			if (OnSoundFinishedPlaying != null)
			{
				OnSoundFinishedPlaying.Invoke(this); // The AudioManager will pick this up
			}
		}

		private void NotifySoundLoopNext()
		{
			if (OnSoundLoopNext != null)
			{
				OnSoundLoopNext.Invoke(this); // The AudioManager will pick this up
			}
		}
	}
}

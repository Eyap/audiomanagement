﻿
namespace GameLibrary.AudioManagement
{
	using UnityEngine;
	using UnityEngine.Audio;

	//TODO: Check which settings we really need at this level
	[CreateAssetMenu(menuName = "Audio/Audio Configuration")]
	public class AudioConfigurationSO : ScriptableObject
	{
		public AudioMixerGroup outputAudioMixerGroup = null;

		// Simplified management of priority levels (values are counterintuitive, see enum below)
		[SerializeField] private PriorityLevel _priorityLevel = PriorityLevel.Standard;
		public int priority
		{
			get { return (int)_priorityLevel; }
			set { _priorityLevel = (PriorityLevel)value; }
		}

		[Header("Sound properties")]
		public bool mute = false;
		[Range(0f, 1f)] public float volume = 1f;
		[Range(-3f, 3f)] public float pitch = 1f;
		[Range(-1f, 1f)] public float panStereo = 0f;
		[Range(0f, 1.1f)] public float reverbZoneMix = 1f;

		[Header("Spatialisation")]
		[Range(0f, 1f)] public float spatialBlend = 1f;
		public AudioRolloffMode rolloffMode = AudioRolloffMode.Logarithmic;
		[Range(0.01f, 5f)] public float minDistance = 0.1f;
		[Range(5f, 100f)] public float maxDistance = 50f;
		[Range(0, 360)] public int spread = 0;
		[Range(0f, 5f)] public float dopplerLevel = 1f;

		private enum PriorityLevel : int
		{
			Highest = 0,
			High = 64,
			Standard = 128,
			Low = 194,
			VeryLow = 256,
		}

		public void ApplyTo(AudioSource audioSource)
		{
			audioSource.outputAudioMixerGroup = this.outputAudioMixerGroup;
			audioSource.mute = this.mute;
			audioSource.priority = this.priority;
			audioSource.volume = this.volume;
			audioSource.pitch = this.pitch;
			audioSource.panStereo = this.panStereo;
			audioSource.spatialBlend = this.spatialBlend;
			audioSource.reverbZoneMix = this.reverbZoneMix;
			audioSource.dopplerLevel = this.dopplerLevel;
			audioSource.spread = this.spread;
			audioSource.rolloffMode = this.rolloffMode;
			audioSource.minDistance = this.minDistance;
			audioSource.maxDistance = this.maxDistance;
		}
	}
}
